from itertools import chain
from collections import namedtuple
from zipfile import ZipFile
from matplotlib.pyplot import imread
import numpy as np

import cv2 as cv
import pandas as pd
import numpy
from imantics import Polygons
from shapely.geometry import Polygon as ShapelyPolygon


class GrabCutter(object):
    @staticmethod
    def cut(window):
        img = window.local_image
        mask = np.zeros(img.shape[:2], dtype=np.uint8)
        rect = window.grabcut_rect(5)

        bgmodel = np.zeros((1, 65), dtype=np.float64)
        fgmodel = np.zeros((1, 65), dtype=np.float64)
        iterations = 3
        mode = cv.GC_INIT_WITH_RECT

        cv.grabCut(img, mask, rect, bgmodel, fgmodel, iterations, mode)
        mask2 = np.where((mask == 2) | (mask == 0), 0, 1).astype('uint8')
        fg_img = img * mask2[:, :, np.newaxis]
        fg_poly = np.array(Polygons.from_mask(mask2).points[0])
        return (fg_poly, fg_img, mask2)
    
    @staticmethod
    def calc_iou(actual, predicted):
        A = actual if isinstance(actual, ShapelyPolygon) else ShapelyPolygon(actual)
        
        # If the predicted polygon contains less than 3 points, something went
        # wrong so it should be a score of 0
        try:
            P = predicted if isinstance(predicted, ShapelyPolygon) else ShapelyPolygon(predicted)
        except ValueError:
            return 0

        # Attempt to fix invalid / self-intersecting polygons
        if not A.is_valid:
            A = A.buffer(0)
        if not P.is_valid:
            P = P.buffer(0)

        intersection = A.intersection(P)
        union_area = A.area + P.area - intersection.area
        return A.area / union_area
