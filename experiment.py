
from matplotlib.pyplot import imread, subplot
import numpy as np

import cv2 as cv
import pandas as pd
import numpy
from imantics import Polygons
from PIL import Image, ImageDraw

from skimage import draw
import pylab as plt
from matplotlib.path import Path
from sklearn.metrics import f1_score

from shapely.geometry import Polygon as ShapelyPolygon
from matplotlib.pyplot import imread, xlabel
from matplotlib.patches import Polygon, Rectangle
from skimage import exposure, color, img_as_ubyte
from skimage.color.adapt_rgb import adapt_rgb, each_channel, hsv_value


@adapt_rgb(each_channel)
def equalize_hist(image):
    return exposure.equalize_hist(image)

def poly2mask(poly_verts, shape):
    img = Image.new('L', (shape[1], shape[0]), 0)
    ImageDraw.Draw(img).polygon(poly_verts, outline=1, fill=1)
    mask = numpy.array(img)
    return mask

def preprocess_image(img, options={}):
    gc_img = None
    if options.get('equalize', False):
        img = equalize_hist(color.rgb2gray(img))
    return ((gc_img if gc_img is not None else img), img)

def grabcut(window, options={}):
    gc_img, img = preprocess_image(window.local_image, options)
    window.set_processed_image(img)
    mask = np.zeros(img.shape[:2], dtype=np.uint8)
    rect, _ = window.grabcut_rect()

    bgmodel = np.zeros((1, 65), dtype=np.float64)
    fgmodel = np.zeros((1, 65), dtype=np.float64)
    iterations = 3  
    mode = cv.GC_INIT_WITH_RECT

    cv.grabCut(gc_img, mask, rect, bgmodel, fgmodel, iterations, mode)
    mask2 = np.where((mask == 2) | (mask == 0), 0, 1).astype('uint8')
    fg_img = gc_img * mask2[:, :, np.newaxis]
    fg_poly = np.array(Polygons.from_mask(mask2).points[0])
    return (fg_poly, fg_img, mask2, window)

def calc_iou(actual, predicted):
    A = (actual if isinstance(actual, ShapelyPolygon)
          else ShapelyPolygon(actual))
    
    # If the predicted polygon contains less than 3 points, something went
    # wrong so it should be a score of 0
    try:
        P = (predicted if isinstance(predicted, ShapelyPolygon)
              else ShapelyPolygon(predicted))
    except ValueError:
        return 0

    # Attempt to fix invalid / self-intersecting polygons
    if not A.is_valid:
        A = A.buffer(0)
    if not P.is_valid:
        P = P.buffer(0)

    intersection = A.intersection(P)
    union_area = A.area + P.area - intersection.area
    return intersection.area / union_area

def calc_fscore(actual_mask, predicted_mask):
    y_true = actual_mask.flatten()
    y_pred = predicted_mask.flatten()
    return f1_score(y_true, y_pred, average='micro')


class Trial(object):
    def __init__(self):
        self.result = None
        self.failed = []

    def run(self, facade, options={}):
        window_results = []
        for window in facade.windows:
            try:
                grabcut_result = grabcut(window, options)
            except IndexError:
                self.failed.append(window)
                continue
            grabcut_result_poly, _, predicted_mask, window = grabcut_result

            actual_mask = poly2mask(window.local_polygon,
                                    window.local_image.shape)
            fscore = calc_fscore(actual_mask, predicted_mask)
            iou = calc_iou(actual=window.local_polygon,
                           predicted=grabcut_result_poly)

            window_results.append(dict(window=window, grabcut_result=grabcut_result,
                                       iou=iou, fscore=fscore))

        iou = sum(r['iou'] for r in window_results) / len(window_results)
        fscore = sum(r['fscore'] for r in window_results) / len(window_results)
        self.result = dict(facade=facade, iou=iou, fscore=fscore, window_results=window_results)

    def plot_local_results(self, plt, show_orig_windows=True):
        plt.subplots_adjust(top=2, bottom=1, left=1, right=2)
        rows, cols = len(self.result['window_results']) // 5 + 1, 5
        for idx, window_result in enumerate(self.result['window_results']):
            ax = subplot(rows, cols, idx + 1)
            window = window_result['window']
            grabcut_result_poly, _, _ , _= window_result['grabcut_result']
            ax.imshow(window.local_image if show_orig_windows else window.processed_image)
            # ax.imshow(exposure.equalize_hist(color.rgb2gray(window.local_image)))
            ax.add_patch(Polygon(grabcut_result_poly, color='red', lw=2, ls='--', fill=False))
            _, rect = window.grabcut_rect()
            # ax.add_patch(Rectangle(*rect, color='red', fill=False, lw=3, ls='--'))
            iou = window_result['iou']
            fscore = window_result['fscore']
            xlabel('Fscore = {:.2f}\nIoU = {:.2f}'.format(fscore, iou), fontdict=dict(size=8))