import json
import xml.etree.ElementTree as ET
import ast
from os import listdir
from collections import namedtuple
from os.path import isfile, join
from zipfile import ZipFile

from shapely.geometry import Polygon as ShapelyPolygon
from matplotlib.pyplot import imread

import pandas as pd
import numpy as np

from grabcut import GrabCutter, poly2mask

IMAGES_PATH = './annotation-tools/psvdata/images/merged/'
ANNOTATIONS_PATH = './annotation-tools/psvdata/Annotations/merged/'

FACADE_IDS = [
    'uYD6sWlLhJByWmX-AAAk1A',
    # 'ZQk5S4pCozThzj8QYYW5UA',
    # 'cMS-heyN46L6jFMPGY-vGg'
]

class WindowData(object):
    _window_data_cache = {}
    
    def __init__(self):
        self._window_data_cache = _load_windows()

    def load_window(self, id_=None):
        if id_ is None:
            record = next(iter(self._window_data_cache.values()))
            id_ = record['id']
        else:
            record = self._window_data_cache[id_]
        return Window(id_, record)

    def load_facades(self):
        facade_annotation_files = []
        for fname in listdir(ANNOTATIONS_PATH):
            for id_ in FACADE_IDS:
                if id_ in fname and fname.endswith('.xml'):
                    facade_annotation_files.append(fname)

        facades = []
        for fname in facade_annotation_files:
            root = ET.parse(join(ANNOTATIONS_PATH, fname)).getroot()
            windows = []
            for type_tag in root.findall('object'):
                artifact = type_tag.find('name')
                if artifact.text == 'window':
                    type_el = type_tag.find('type')
                    if type_el is not None:
                        id_ = type_tag.find('id').text
                        window = self.load_window(id_)
                        window.global_polygon = self._get_poly_from_xml(type_tag)
                        windows.append(window)
            image = imread(join(IMAGES_PATH, root.find('filename').text))
            facades.append(Facade(image, windows))    
        return facades

    def _get_point(self, xvals, yvals, ndx):
        return float(xvals[ndx].text), float(yvals[ndx].text)

    def _get_poly_from_xml(self, obj):
        xvals = obj.findall('polygon/pt/x')
        yvals = obj.findall('polygon/pt/y')
        polygon = ShapelyPolygon([self._get_point(xvals, yvals, 0),
                                  self._get_point(xvals, yvals, 1),
                                  self._get_point(xvals, yvals, 2),
                                  self._get_point(xvals, yvals, 3)])
        return [self._get_point(xvals, yvals, 0),
                self._get_point(xvals, yvals, 1),
                self._get_point(xvals, yvals, 2),
                self._get_point(xvals, yvals, 3)]
        # return polygon


class Window(object):
    def __init__(self, id_, record):
        self.id = id_
        self.local_image = imread(f'./windows/windows/{self.id}.jpg')
        self.local_polygon = np.array(ast.literal_eval(record['local_polygon']))
        self.processed_image = None

    def grabcut_rect(self, ratio=(1/3, 1/5)):
        """Using the annotations as a starting point, return
        the rectangle required for grabcut in the format that
        cv2 expects (x1 y1 x2 y2),
        """
        # points = [(x, y) for x, y in poly.exterior.coords]
        xvals, yvals = zip(*self.local_polygon)
        x1, y1 = min(xvals), min(yvals)
        x2, y2 = max(xvals), max(yvals)

        width, height = x2 - x1, y2 - y1
        x1 -= width * ratio[0]
        y1 -= height * ratio[1]
        x2 += width * ratio[0]
        y2 += height * ratio[1]

        return (tuple(int(v) for v in [x1, y1, x2, y2]),
                ((x1, y1), x2 - x1, y2 - y1))
    
    def set_processed_image(self, img):
        self.processed_image = img


class Facade(object):
    def __init__(self, image, windows):
        self.image = image
        self.windows = windows


def _index_windows():
    data = ZipFile('./windows.zip')
    meta = pd.read_csv(data.open('windows.csv'), index_col='id')
    cache = {id_: dict(id=id_, local_polygon=record.local_polygon)
             for id_, record in meta.iterrows()}
    with open('./windows.json', 'w') as jsonfile:
        json.dump(cache, jsonfile)

def _load_windows():
    with open('./windows.json') as jsonfile:
        d = json.load(jsonfile)
        return d

if __name__ == '__main__':
    _index_windows()